import hashlib
import json
import os

import requests
from transcoder import api_base


class FileIsDirError(OSError):
    """ File is a directory. """
    pass


class FileAlreadyUploadedError(Exception):
    """ File has been fully uploaded and it cannot be modified anymore. """
    pass


class IllegalArgumentError(ValueError):
    pass


class ApiException(Exception):
    """ Server error. """
    api_response = None

    def __init__(self, api_response):
        super().__init__(api_response["data"][0])
        self.api_response = api_response


class FilePart:
    """Represents a part of a file that is being uploaded in chunks."""
    def __init__(self, uuid, size, name, creation_date, range_start):
        self.uuid = uuid
        self.size = size
        self.name = name
        self.range_start = range_start
        self.creation_date = creation_date

    def json(self):
        return json.dumps(self.__dict__)

    @classmethod
    def from_dict(cls, json_dict):
        return cls(
            json_dict["uuid"],
            json_dict["size"],
            json_dict["name"],
            json_dict["creationDate"],
            json_dict["rangeStart"],
        )

    @classmethod
    def from_json(cls, json_str):
        return cls.from_dict(json.loads(json_str))


class File:
    """ Represents a file stored in Transcoder cloud storage. """
    def __init__(self, uuid, size, name, dir, creation_date, user_id, delete_after_download, is_available, parts, hash):
        self.uuid = uuid
        self.size = size
        self.name = name
        self.dir = dir
        self.creation_date = creation_date
        self.user_id = user_id
        self.delete_after_download = delete_after_download
        self.is_available = is_available
        self.parts = parts
        self.hash = hash

    def json(self):
        return json.dumps(self.__dict__)

    @classmethod
    def from_json(cls, json_str):
        json_dict = json.loads(json_str)
        return cls(
            json_dict["uuid"],
            json_dict["size"],
            json_dict["name"],
            json_dict["dir"],
            json_dict["creationDate"],
            json_dict["userId"],
            json_dict["deleteAfterDownload"],
            json_dict["isAvailable"],
            list(map(FilePart.from_dict, json_dict["parts"])),
            json_dict["hash"],
        )


class Uploader:
    """Used to upload new files to the transcoder cloud storage."""
    __file_info_api_endpoint = api_base + "v1/browse/search?apiKey=%s&fileName=%s&dir=%s"
    __file_upload_api_endpoint = api_base + "v1/upload/chunks?apiKey=%s"

    def __init__(self, api_key):
        self.api_key = api_key

    @staticmethod
    def __check_part(file, start_range, end_range) -> FilePart or None:
        end_range -= 1
        for part in file.parts:
            part.range_end = part.range_start + part.size - 1
            if start_range >= part.range_start and start_range <= part.range_end:
                return part
            elif end_range >= part.range_start and end_range <= part.range_end:
                return part
            elif part.range_start <= start_range and end_range <= part.range_start:
                return part
            elif start_range >= part.range_start and end_range <= part.range_end:
                return part
            elif start_range <= part.range_start and end_range >= part.range_end:
                return part

    def get_file_info(self, file_name, server_dir) -> File or None:
        """Returns details about a chosen file"""
        if file_name is None or file_name == "":
            raise IllegalArgumentError("file_name is required")
        if server_dir is None or server_dir == "":
            raise IllegalArgumentError("server_dir is required")
        if not isinstance(file_name, str):
            raise ValueError("file_name must be a string")
        if not isinstance(server_dir, str):
            raise ValueError("server_dir must be a string")
        r = requests.get(self.__file_info_api_endpoint % (self.api_key, file_name, server_dir))
        data = r.json()
        if data["status"] == "ok":
            return File.from_json(json.dumps(data["data"][0]))
        elif data["status"] == "notfound":
            return None
        else:
            raise ApiException(data)

    def upload(self, file_path, server_dir, chunk_size):
        """
        Used to upload a file in small chunks rather than all at once.
        If transfer is interrupted then once you try to upload the same
        file it will resume starting from the last chunk that was uploaded.
        """
        Uploader.__validate(file_path, server_dir)

        if not os.path.exists(file_path):
            raise FileNotFoundError(file_path)
        if not os.path.isfile(file_path):
            raise FileIsDirError("file_path is not a file")

        file_name = os.path.basename(file_path)
        file = self.get_file_info(file_name, server_dir)
        if file is not None and file.is_available:
            raise FileAlreadyUploadedError("this file is already fully uploaded")

        file_size = os.stat(file_path).st_size
        start = 0
        end = chunk_size

        while end <= file_size:
            if file is not None:
                file_part = Uploader.__check_part(file, start, end)
                while file_part is not None and end <= file_size:
                    if start < file_part.range_start and end > (start + file_part.size):
                        end = file_part.range_start
                    else:
                        start += file_part.size
                        end = start + chunk_size
                    file_part = Uploader.__check_part(file, start, end)

            self.upload_part(file_path, server_dir, start, end - start)
            start = end
            if start >= file_size:
                break

            end = start + chunk_size
            if end >= file_size:
                end = file_size

        pass

    @staticmethod
    def __validate(file_path, server_dir):
        if file_path is None or file_path == "":
            raise IllegalArgumentError("file_path is required")
        if server_dir is None or server_dir == "":
            raise IllegalArgumentError("server_dir is required")
        if not isinstance(file_path, str):
            raise ValueError("file_path must be a string")
        if not isinstance(server_dir, str):
            raise ValueError("server_dir must be a string")

    @staticmethod
    def __generate_hash(file_path):
        length = 4096
        file_size = os.stat(file_path).st_size
        if file_size < length:
            length = file_size
        f = open(file_path, 'rb')
        data = f.read(length)
        return hashlib.md5(data).hexdigest()

    def upload_part(self, file_path, server_dir, offset=0, length=512):
        """Allows to upload a chunk of a file.
        This function will not check whether this file is already fully uploaded,
        if it's already uploaded server will return an error."""
        Uploader.__validate(file_path, server_dir)

        try:
            if not os.path.isfile(file_path):
                raise FileIsDirError("file_path is not a file")

            f = open(file_path, 'rb')
            f.seek(offset)

            multipart_form_data = {
                'file': ('%s.part' % os.path.basename(file_path), f.read(length)),
                'fileName': (None, os.path.basename(file_path)),
                'hash': (None, Uploader.__generate_hash(file_path)),
                'fileSize': (None, str(os.stat(file_path).st_size)),
                'dir': (None, server_dir),
                'start': (None, str(offset)),
            }

            response = requests.post(self.__file_upload_api_endpoint % self.api_key, files=multipart_form_data)
            print(response.json())
        except FileNotFoundError as e:
            raise e
        finally:
            f.close()

